# 토픽 모델링: LDA와 NMF

## <a name="intro"></a> 개요
토픽 모델링은 대량의 텍스트 문서 모음에 존재하는 숨겨진 주제나 주제를 발견하는 기법이다. 토픽 모델링은 대량의 텍스트 데이터를 효율적이고 의미 있는 방법으로 분석하고 탐색하며 이해하는 데 도움을 줄 수 있다.

이 포스팅에서는 잠재 디리클레 할당(LDA, Latent Dirichlet Allocation)와 비음수 행렬 분해(NMF, Non-Negative Matrix Factorization)라는 잘 알려진 두 기법을 사용하여 텍스트 데이터에 대한 토픽 모델링을 수행하는 방법을 설명할 것이다. 또한 다양한 메트릭과 시각화 도구를 사용하여 이러한 기법의 결과를 비교하고 평가하는 방법도 다룰 것이다.

이 포스팅으로 다음을 학습할 수 있다.

- 토픽 모델링이란 무엇이며 텍스트 분석에 유용한 이유를 설명한다.
- 토픽 모델링을 위해 텍스트 데이터를 전처리하고 벡터화한다.
- 텍스트 데이터에서 주제를 찾기 위해 LDA와 NMF 기법을 적용한다.
- 토픽 모델링 결과를 해석하고 시각화한다.
- LDA 기법과 NMF 기법의 성능을 비교 평가한다.

이 글을를 학습하려면 Python과 데이터 분석에 대한 기본적인 이해가 필요하다. 또한 다음과 같은 라이브러리를 설치해야 한다.

- Numpy: Python의 과학 컴퓨팅을 위한 라이브러리
- Pandas: Python에서 데이터 조작과 분석을 위한 라이브러리
- Scikit-learn: Python에서 기계 학습을 위한 라이브러리
- Gensim: Python에서 토픽 모델링과 자연어 처리를 위한 라이브러리
- PyLDAvis: Python에서 토픽 모델의 대화형 시각화를 위한 라이브러리

pip 명령을 사용하여 이러한 라이브러리를 설치한다.

```bash
$ pip install numpy
$ pip install pandas
$ pip install scikit-learn
$ pip install gensim
$ pip install pyldavis
```

또는 Google Colab이나 Kaggle과 같은 클라우드 기반 플랫폼을 사용하여 로컬 컴퓨터에 아무것도 설치하지 않고 이 포스팅의 코드를 실행할 수 있다.

토픽 모델링에 들어갈 준비가 되었나요? 시작해 보겠다!

## <a name="sec_02"></a> 토픽 모델링이란?
토픽 모델링은 많은 텍스트 문서 모음 속에 숨어 있는 토픽이나 주제를 찾아내는 기법이다. 토픽은 자주 등장하는 단어들의 모음으로, 특정 개념이나 아이디어를 나타낸다. 예를 들어, 스포츠와 관련된 토픽은 "축구", "농구", "득점", "팀" 등의 단어를 포함할 수 있다. 정치와 관련된 토픽은 "선거", "대통령", "정당", "투표" 등의 단어를 포함할 수 있다.

토픽 모델링은 대량의 텍스트 데이터를 효율적이고 의미 있는 방식으로 분석, 탐색, 이해하는 데 도움을 줄 수 있다. 토픽 모델링의 응용 사례로는 다음과 같은 것들이 있다.

- **텍스트 요약**: 토픽 모델링을 사용하여 텍스트 문서 또는 문서 말뭉치에서 주요 토픽 또는 주제를 추출하고 내용을 간략하게 요약할 수 있다.
- **텍스트 분류**: 토픽 모델링을 사용하여 텍스트 문서에 포함된 토픽을 기반으로 레이블이나 범주를 지정할 수 있다. 예를 들어, 뉴스 기사를 스포츠, 정치, 엔터테인먼트 등 다양한 장르로 분류할 수 있다.
- **텍스트 클러스터링**: 토픽 모델링을 사용하면 공유하는 토픽을 기반으로 유사한 텍스트 문서를 그룹화할 수 있다. 예를 들어 고객 리뷰를 언급하는 측면이나 피처를 기반으로 클러스터링할 수 있다.
- **텍스트 추천**: 토픽 모델링을 사용하여 사용자의 선호 또는 관심사를 기반으로 관련 텍스트 문서를 사용자에게 추천할 수 있다. 예를 들어, 사용자가 좋아하는 토픽을 기반으로 책, 기사 또는 블로그를 사용자에게 추천할 수 있다.
- **텍스트 검색**: 토픽 모델링을 사용하여 텍스트 검색 결과의 품질과 관련성을 향상시킬 수 있다. 예를 들어, 토픽 모델링을 사용하여 검색 결과가 일치하는 토픽을 기반으로 검색 결과를 필터링하거나 순위를 매길 수 있다.

토픽 모델링에는 여러 가지 다양한 기술과 알고리즘이 있지만, 본 포스팅에서는 가장 대중적이고 널리 사용되는 두 기술인 잠재 디리클레 할당(LDA)과 비음수 행렬 분해(NMF)에 초점을 맞출 것이다.

LDA와 NMF는 무엇이고 어떻게 작동하는가? 다음 절에서 알아본다.

## <a name="sec_03"></a> 잠재 디리클레 할당(LDA)
잠재 디리클레 할당(Latent Dirichlet Allocation, LDA)은 2003년 David Blei, Andrew Ng과 Michael Jordan이 도입한 토픽 모델링을 위한 확률론적 기법이다. LDA는 말뭉치의 각 문서가 토픽들의 혼합물이며, 각 토픽들이 단어들의 분포라는 가정에 기초하고 있다. LDA는 각 토픽에 대한 단어 확률뿐만 아니라 잠재 토픽들과 각 문서에서 차지하는 비율을 발견하는 것을 목표로 한다.

LDA의 주요 아이디어는 각 문서를 토픽 비율의 벡터로 나타낼 수 있고, 각 토픽을 단어 확률의 벡터로 나타낼 수 있다는 것이다. 예를 들어, 3개의 문서로 구성된 말뭉치를 가지고 있다고 가정하자.

- Document 1: "I like to play football and basketball."
- Document 2: "I love to watch movies and listen to music."
- Document 3: "I enjoy both sports and entertainment."

LDA는 이 말뭉치에서 스포츠와 관련된 주제와 엔터테인먼트와 관련된 주제 두 가지를 발견할 수 있다. 각 문서의 주제 비율은 다음과 같이 보일 수 있다.

![](./images/table_01.webp)

각 토픽에 대한 단어의 확률은 다음과 같이 보일 수 있다.

![](./images/table_02.webp)

이러한 토픽 비율과 단어 확률을 이용하여 어느 정도의 정확성을 가지고 원본 문서를 재구성할 수 있다. 예를 들어, Document 1은 다음과 같이 재구성될 수 있다.

> <font size="3">"I like to play <b>football</b> and <b>basketball</b>."</font>

굵은 글씨들은 Document 1에서 지배적인 주제(Sports)에 대한 확률이 가장 높은 단어들이다. 다른 단어들은 두 토픽 모두에 대한 확률이 더 낮으므로, 덜 유익하다.

LDA는 이러한 토픽과 확률을 어떻게 발견하는가? LDA는 토픽과 단어로부터 문서가 어떻게 생성되는지 모델링하기 위해 생성 프로세스를 사용한다. 생성 프로세스는 다음과 같다.
1. 각 토픽에 대해 매개변수 베타(beta)를 사용하여 Dirichlet 분포로부터 단어 분포를 그린다.
1. 각 문서에 대해 매개변수 알파(alpha)를 사용하여 Dirichlet 분포로부터 토픽 분포를 그린다.
1. 문서의 각 단어에 대해 토픽 분포에서 토픽 할당을 그린다.
1. 할당된 토픽의 단어 분포에서 단어를 끌어낸다.

Dirichlet 분포는 유한한 결과 집합의 확률을 모델링하는 데 사용할 수 있는 베타 분포의 다변량 일반화이다. 파라미터 알파와 베타는 분포의 모양과 희소성을 제어하는 하이퍼파라미터이다. 알파 값이 높으면 문서에 토픽이 균일하게 분포되어 있을 가능성이 높은 반면, 알파 값이 낮으면 문서에 토픽이 편향된 분포가 있을 가능성이 높다는 것을 의미한다. 베타 값이 높으면 토픽에 단어가 균일하게 분포되어 있을 가능성이 높은 반면, 베타 값이 낮으면 토픽에 단어가 편향된 분포가 있을 가능성이 높다는 것을 의미한다.

생성 과정은 우리가 토픽의 개수, 알파와 베타의 값, 각 문서의 길이 등을 알고 있다고 가정한다. 그러나 실제로는 문서와 단어만을 관찰하여 숨겨진 토픽과 확률을 추론하고자 한다. 여기서 LDA는 베이지안 추론이라는 기법을 사용하는데, 베이지안 추론은 관측된 데이터를 기반으로 미지의 파라미터에 대한 우리의 믿음을 업데이트하는 방법이다. LDA는 베이지안 추론을 위해 붕괴 깁스(collapsed Gibbs) 샘플링이라는 특정 알고리즘을 사용한다. 이는 문서와 단어가 주어진 상태에서 토픽 할당의 사후 분포와 단어와 토픽 분포로부터 표본을 추출하는 방법이다.

LDA에 대한 붕괴된 깁스 샘플링 알고리즘은 다음과 같다.

1. 각 문서의 각 단어에 대한 토픽 할당을 무작위로 초기화한다.
1. 각 문서의 각 단어에 대해 다음을 수행한다.
    - 단어에 대한 현재 토픽 할당을 제거한다.
    - 단어에 대한 각 토픽의 조건부 확률은 문서와 단어에 대한 현재 토픽 할당을 기준으로 계산한다. 조건부 확률은 문서에서 토픽에 할당된 단어의 비율과 토픽에 해당하는 단어의 비율이라는 두 항의 곱에 비례한다.
    - 조건부 확률 분포에서 단어에 대한 새 토픽 할당을 샘플링한다.
    - 단어에 대한 토픽 할당을 업데이트한다.
1. 수렴하거나 최대 반복 횟수가 될 때까지 2단계를 반복한다.
1. 최종 토픽 과제를 바탕으로 단어와 토픽 분포를 계산한다.

깁스 샘플링 알고리즘의 출력은 각 문서의 각 단어에 대한 토픽 할당의 집합과 단어와 토픽 분포의 집합이다. 이들은 각 문서에서 토픽과 그 비율, 토픽에 대한 단어 확률 등을 해석하는 데 사용될 수 있다.

우리는 Python에서 LDA를 어떻게 구현할 수 있을까? 다행히 LDA 기능을 제공하는 기존 라이브러리들이 존재하기 때문에 처음부터 깁스 샘플링 알고리즘을 코딩할 필요가 없다. 그중 하나가 Gensim이다. Gensim은 Python에서 토픽 모델링과 자연어 처리를 위한 라이브러리이다. Gensim은 LdaModel이라는 클래스를 제공한다. 이 클래스는 문서 말뭉치로 LDA 모델을 생성하고 훈련하는 데 사용할 수 있다. `LdaModel` 클래스는 토픽과 토픽의 확률을 질의하고 시각화하는 방법도 제공한다.

다음 절에서는 Gensim을 사용하여 텍스트 문서의 실제 데이터 세트에 LDA를 적용하는 방법을 알아본다.

## <a name="sec_04"></a> 비음수 행렬 분해(NMF)
비음수 행렬 분해(Non-negative Matrix Factorization, NMF)는 1999년 Daniel Lee와 Sebastian Seung이 도입한 토픽 모델링을 위한 또 다른 기법이다. NMF는 말뭉치의 각 문서가 토픽의 선형 조합에 의해 근사화될 수 있고, 각 토픽이 단어의 선형 조합에 의해 근사화될 수 있다는 가정에 기초한다. NMF는 각 문서에서 최적의 토픽과 그 가중치, 그리고 각 토픽에서 최적의 단어와 그 가중치를 찾는 것을 목표로 한다.

NMF의 주요 아이디어는 각 문서를 단어 빈도의 벡터로 나타낼 수 있고, 각 토픽을 단어 가중치의 벡터로 나타낼 수 있다는 것이다. 예를 들어, 이전과 동일한 3 문서의 말뭉치가 있다고 가정하자.

- Document 1: "I like to play football and basketball."
- Document 2: "I love to watch movies and listen to music."
- Document 3: "I enjoy both sports and entertainment."

우리는 각 문서를 단어 빈도의 벡터로 나타낼 수 있으며, 여기서 각 원소는 단어가 문서에 나타나는 횟수에 해당한다. 예를 들어, Document 1은 다음과 같이 나타낼 수 있다.

```
[1, 1, 1, 1, 0, 0, 0, 0, 0, 0]
```

원소들의 순서는 말뭉치에 있는 단어들의 알파벳 순서에 기초한다. 첫 번째 원소는 "and"의 빈도이고, 두 번째 원소는 "basketball"의 빈도 등이다. 마찬가지로, 우리는 다른 문서들을 단어 빈도의 벡터로 나타낼 수 있다.

우리는 각 토픽 단어 가중치 벡터로 나타낼 수도 있다. 각 원소는 토픽에 대한 단어의 중요도나 관련성에 해당한다. 예를 들어, 스포츠와 관련된 토픽은 다음과 같이 나타낼 수 있다.

```
[0.1, 0.4, 0.4, 0.1, 0, 0, 0, 0, 0, 0]
```

원소들의 순서는 앞과 같다. 첫 번째 원소는 "and"의 가중치, 두 번째 원소는 "basketball"의 가중치 등이다. 마찬가지로 entertainment와 관련된 주제를 단어 가중치의 벡터로 나타낼 수 있다.

이러한 단어 빈도와 단어 가중치들을 사용하여, 우리는 토픽의 선형 조합으로서 각각의 문서를 근사화할 수 있다. 예를 들어, Document 1을 다음과 같이 근사화할 수 있다.

$Document1 = 0.8 * Sports + 0.2 * Entertainment$


계수 0.8과 0.2는 문서에 있는 주제들의 가중치이다. 그들은 각 주제가 문서에 얼마나 기여하는지 나타낸다. 근사치는 행렬 형태로 다음과 같이 쓸 수 있다.

$D = W*H$

여기서, $D$는 document-term 행렬, $W$는 document-topic 행렬, $H$는 topic-term 행렬이다. document-term 행렬 $D$는 단어 빈도의 행렬로서, 각 행은 문서에 대응하고 각 열은 단어에 대응한다. document-topic 행렬 $W$는 토픽 가중치의 행렬로서, 각 행은 문서에 대응하고 각 열은 토픽에 대응한다. 토픽-용어 행렬 $H$는 단어 가중치들의 행렬로서, 각 행은 토픽에 대응하고 각 열은 단어에 대응한다.

NMF는 어떻게 행렬 $D$에 가장 근접한 최적의 행렬 $W$와 $H$를 찾을까? NMF는 교대 최소 제곱(alternating least squares)이라 불리는 최적화 기법을 사용한다. 이는 원래 행렬 $D$와 근사 행렬 $W * H$ 사이의 재구성 오차를 최소화하는 방법이다. 재구성 오차는 두 행렬 사이의 거리를 계산하는 방법인 Frobenius 노름에 의해 측정된다. Frobenius 노름은 다음과 같이 정의된다.

$\lVert{\mathbf{D} - \mathbf{W}*\mathbf{H}}\rVert F = \sqrt{\sum_k{(\mathbf{D}_{i,j} - \mathbf{W}_{i,k}*\mathbf{H}_{k,j}})^2}$

NMF의 교대 최소 제곱 알고리즘은 다음과 같다.

1. 음이 아닌 값으로 행렬 $\mathbf{W}$와 $\mathbf{H}$를 임의로 초기화한다.
1. $\mathbf{H}$에 대하여 $\lVert{\mathbf{D} - \mathbf{W}*\mathbf{H}}\rVert F$를 최소화하여 최소 제곱 문제를 해결함으로써 $\mathbf{W}$를 고정하고 $\mathbf{H}$를 업데이트한다.
1. $\mathbf{W}$에 대하여 $\lVert{\mathbf{D} - \mathbf{W}*\mathbf{H}}\rVert F$를 최소화하여 최소 제곱 문제를 해결함으로써 $\mathbf{H}$$\mathbf{W}$ 고정하고 W를 업데이트한다.
1. 수렴하거나 최대 반복 횟수가 될 때까지 2단계와 3단계를 반복한다.

교대 최소제곱 알고리즘의 출력은 행렬 $\mathbf{D}$에 가장 가까운 행렬 $\mathbf{W}$와 $\mathbf{H}$의 쌍이다. 이들은 각 문서에서 토픽과 그 가중치, 각 토픽에서 단어와 그 가중치를 해석하는 데 사용할 수 있다.

우리는 Python에서 NMF를 어떻게 구현할 수 있을까? 다행히도 우리는 NMF 기능을 제공하는 기존 라이브러리들이 존재하기 때문에 처음부터 교대 최소제곱 알고리즘을 코딩할 필요가 없다. 그 중 하나가 Scikit-learn이다. 이는 Python에서 기계 학습을 위한 라이브러리이다. Scikit-learn은 NMF라는 클래스를 제공하는데, 이것은 단어 빈도 행렬에 NMF 모델을 생성하고 훈련하는 데 사용될 수 있다. `NMF` 클래스는 토픽과 그 확률을 쿼리하고 시각화하는 방법도 제공한다.

다음 절에서는 Scikit-learn을 사용하여 이전과 동일한 텍스트 문서 데이터세트에 NMF를 적용하는 방법을 알아본다.

## <a name="sec_05"></a> LDA와 NMF 비교평가
본 절에서는 동일한 텍스트 문서 데이터 세트에 대한 LDA와 NMF의 성능을 비교 평가할 것이다. 20개의 다양한 토픽의 약 20,000개의 뉴스 그룹 게시물을 모은 20개의 뉴스 그룹 데이터세트를 사용할 것이다. 데이터 세트는 Scikit-Learn에 내장된 기능으로 제공된다.

LDA와 NMF를 비교 평가하기 위해 다음 단계를 사용한다.

- 데이터세트을 로드하고 전처리한 후 TF-IDF를 이용하여 텍스트 데이터를 벡터화한다.
- 벡터화된 데이터에 대해 LDA와 NMF 모델을 생성하고 훈련한다.
- 토픽과 그 가능성을 해석하고 시각화한다.
- 각 모델에 대한 일관성(coherence) 점수와 난감도(perplexity) 점수를 계산한다.
- 각 모델의 장단점을 분석한다.

먼저 데이터세트을 로드하고 전처리하는 첫 번째 단계부터 시작하겠다.

### 단계 1: TF-IDF를 이용하여 데이터세트 로드, 전처리 및 텍스트 데이터 벡터화
먼저 20개의 뉴스그룹 데이터세트을 로드하고 텍스트 데이터를 전처리한다. 텍스트를 정규화하기 위해 중지어, 구두점을 제거하고 표제어 추출(lemmatization)을 수행한다.

```python
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import LatentDirichletAllocation, NMF
from sklearn.preprocessing import Normalizer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
import string
import numpy as np
import matplotlib.pyplot as plt
from wordcloud import WordCloud

# Load the 20 Newsgroups dataset
newsgroups_train = fetch_20newsgroups(subset='train', remove=('headers', 'footers', 'quotes'))
# Define stopwords, punctuation, and lemmatizer
stop_words = set(stopwords.words('english'))
lemmatizer = WordNetLemmatizer()
translator = str.maketrans('', '', string.punctuation)
# Function to preprocess text
def preprocess_text(text):
    tokens = word_tokenize(text.lower())  # Tokenize and convert to lowercase
    tokens = [lemmatizer.lemmatize(token) for token in tokens if token.isalpha()]  # Lemmatize and remove non-alphabetic tokens
    tokens = [token for token in tokens if token not in stop_words]  # Remove stopwords
    return ' '.join(tokens)
# Preprocess the text data
preprocessed_data = [preprocess_text(doc) for doc in newsgroups_train.data]
# Vectorize the preprocessed data using TF-IDF
vectorizer = TfidfVectorizer(max_features=10000, stop_words='english')
tfidf_matrix = vectorizer.fit_transform(preprocessed_data)
print("Shape of TF-IDF matrix:", tfidf_matrix.shape)
```

이 코드에서 20개의 뉴스그룹 데이터세트을 로드하고 불용어, 문장부호, 표재어 추출를 정의한 후 토큰화, 소문자 변환, 표재어 추출, 불용어 제거 등의 방법으로 텍스트 데이터를 전처리하였다. 마지막으로 TF-IDF를 사용하여 전처리된 데이터를 벡터화하였다.

### 단계 2: LDA와 NMF 모델 생성과 훈련
다음으로 벡터화된 데이터에 대해 LDA와 NMF 모델을 생성하고 훈련한다.

```python
# Create and train LDA model
lda_model = LatentDirichletAllocation(n_components=20, random_state=42)
lda_topics = lda_model.fit_transform(tfidf_matrix)

# Create and train NMF model
nmf_model = NMF(n_components=20, init='random', random_state=42)
nmf_topics = nmf_model.fit_transform(tfidf_matrix)
```

### 단계 3: 토픽 해석과 시각화
토픽별 상위 단어를 표시하여 토픽을 해석하고 시각화할 수 있다.

```python
def display_topics(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        print(f"Topic {topic_idx + 1}:")
        print(" ".join([feature_names[i] for i in topic.argsort()[:-n_top_words - 1:-1]]))
        print()

n_top_words = 10
tfidf_feature_names = np.array(vectorizer.get_feature_names_out())
print("LDA Topics:")
display_topics(lda_model, tfidf_feature_names, n_top_words)
print("\nNMF Topics:")
display_topics(nmf_model, tfidf_feature_names, n_top_words)
```

### 단계 4: 일관성 점수 및 난감도 점수 계산
우리는 각 모델에 대한 일관성 점수와 난감도 점수를 계산한다.

```python
from sklearn.metrics import pairwise_distances_argmin_min
from sklearn.feature_extraction.text import CountVectorizer
from gensim.models.coherencemodel import CoherenceModel
import gensim

# Convert TF-IDF matrix to document-term matrix
tfidf_model = gensim.models.TfidfModel(dictionary=gensim.corpora.Dictionary(preprocessed_data))
corpus = [tfidf_model[dictionary.doc2bow(doc.split())] for doc in preprocessed_data]
corpus_bow = gensim.matutils.corpus2csc(corpus).transpose()
# Compute coherence score for LDA model
lda_cm = CoherenceModel(model=lda_model, texts=preprocessed_data, corpus=corpus, dictionary=dictionary, coherence='c_v')
lda_coherence = lda_cm.get_coherence()
# Compute coherence score for NMF model
nmf_cm = CoherenceModel(model=nmf_model, texts=preprocessed_data, corpus=corpus, dictionary=dictionary, coherence='c_v')
nmf_coherence = nmf_cm.get_coherence()
print(f"LDA Coherence Score: {lda_coherence}")
print(f"NMF Coherence Score: {nmf_coherence}")
# Compute perplexity score for LDA model
lda_perplexity = lda_model.perplexity(tfidf_matrix)
print(f"LDA Perplexity Score: {lda_perplexity}")
# Compute perplexity score for NMF model
nmf_perplexity = np.exp(nmf_model.reconstruction_err_ / len(tfidf_matrix.toarray()))
print(f"NMF Perplexity Score: {nmf_perplexity}")
```

### 단계 5: 강점과 약점 분석
**잠재 디리클레 할당(LDA)**:

강점:
- 텍스트 데이터의 기본 의미 구조를 캡처한다.
- 문서에서 잠재된 주제를 찾을 수 있다.

약점:
- 토픽의 복잡한 특성으로 인해 해석이 어려울 수 있다.
- 토픽 수와 같은 하이퍼파라미터를 조정해야 한다.

**비음수 행렬 분해(NMF)**:

강점:
- 보다 해석 가능한 주제를 생성한다.
- 토픽 선택과 관련된 하이퍼파라미터를 조정할 필요가 없다.

약점:
- 근본적인 의미 구조를 LDA만큼 효과적으로 포착하지 못할 수 있다.
- LDA보다 계산 비용이 많이 든다.

## <a name="summary"></a> 마치며
이 포스팅에서는 LDA(Latent Dirichlet Allocation)와 NMF(Non-Negative Matrix Factorization)라는 두 기법을 사용하여 텍스트 데이터에 대한 토픽 모델링을 수행하는 방법을 설명하였다. 또한 다양한 메트릭과 시각화 도구를 사용하여 이러한 기법의 결과를 비교하고 평가하는 방법도 알아보았다.

여기에서 설명한 요점은 다음과 같다.
- 토픽 모델링은 방대한 텍스트 문서 모음에 존재하는 숨겨진 토픽이나 토픽을 발견하기 위한 기법이다.
- LDA는 각 문서가 토픽의 혼합물이며, 각 토픽은 단어의 분포라고 가정하는 확률론적 기법이다.
- NMF는 각 문서가 토픽들의 선형 조합에 의해 근사화될 수 있고, 각 토픽이 단어들의 선형 조합에 의해 근사화될 수 있다고 가정하는 최적화 기법이다.
- LDA와 NMF는 각각 라이브러리 Gensim과 Scikit-learn을 사용하여 Python으로 구현할 수 있다.
- 토픽과 확률은 LdaModel과 NMF 클래스에서 제공하는 방법과 PyLDAvis 라이브러리를 사용하여 해석하고 시각화할 수 있다.
- 일관성 점수와 난독성 점수는 토픽 모델의 품질과 적합도를 측정하는 데 사용될 수 있다.
- LDA와 NMF는 데이터와 과제에 따라 장단점이 다르다. LDA는 문서의 생성 과정을 모델링하는 데 더 적합한 반면, NMF는 문서의 잠재 구조를 찾는 데 더 적합하다.

토픽 모델링은 대량의 텍스트 데이터를 효율적이고 의미 있는 방식으로 분석, 탐색 및 이해하는 데 도움을 줄 수 있는 강력하고 다재다능한 기법이다. 텍스트 요약, 텍스트 분류, 텍스트 클러스터링, 텍스트 추천, 텍스트 검색 등 다양한 영역과 응용 프로그램에 토픽 모델링을 적용할 수 있다.

<span style="color:red">실습 코드 수행 필요</span>
