# 토픽 모델링: LDA와 NMF <sup>[1](#footnote_1)</sup>

> <font size="3">LDA와 NMF 기법을 사용하여 텍스트 데이터에 대한 토픽 모델링을 수행하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./topic-modeling.md#intro)
1. [토픽 모델링이란?](./topic-modeling.md#sec_02)
1. [잠재 디리클레 할당(LDA)](./topic-modeling.md#sec_03)
1. [비음수 행렬 분해(NMF)](./topic-modeling.md#sec_04)
1. [LDA와 NMF 비교평가](./topic-modeling.md#sec_05)
1. [마치며](./topic-modeling.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 27 — Topic Modeling: LDA and NMF](https://medium.datadriveninvestor.com/ml-tutorial-27-topic-modeling-lda-and-nmf-ecdb08f8ab61?sk=77f97f8b66339cf8ad2efd54ffba43aa)를 편역하였습니다.
